package com.example.alunos.lcoolgasolina;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    TextView alcool;
    TextView gasolina;

    EditText valor_gasolina;
    EditText valor_alcool;

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        alcool = (TextView) findViewById(R.id.textView2);
        gasolina = (TextView) findViewById(R.id.textView);

        valor_alcool = (EditText) findViewById(R.id.editText);
        valor_gasolina = (EditText) findViewById(R.id.editText3);

        btn = (Button) findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //recuperar os valore digitados
                String textoPrecoAlcool = valor_alcool.getText().toString();
                String textoPrecoGasolina = valor_gasolina.getText().toString();

                //converter valores para numeros
                Double valorAlcool = Double.parseDouble(textoPrecoAlcool);
                Double valorGasolina = Double.parseDouble(textoPrecoGasolina);

                //teste para alcool ou gasolina
                double resultado = valorAlcool / valorGasolina;

                if (resultado >= 0.7) {
                    //Para usar gasolina
                    Toast.makeText(getApplicationContext(), "É melhor usar gasolina!", Toast.LENGTH_LONG).show();
                } else {
                    //Para usar alcool
                    Toast.makeText(getApplicationContext(), "É melhor usar álcool!", Toast.LENGTH_LONG).show();

                }
            }
            });
}
}